package com.basics.day4;

public class Account {
	private String customerName;
	private int accountNo;
	
	public Account()
	{
		
	}
	public Account(String customerName, int accountNo) {
		this.customerName = customerName;
		this.accountNo = accountNo;
	}
	
	void display()
	{
		System.out.println(customerName+"::"+accountNo);
	}
	
	
}
