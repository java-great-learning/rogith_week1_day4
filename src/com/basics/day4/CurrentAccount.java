package com.basics.day4;

public class CurrentAccount extends Account{
	
	private int currentBalance;
	
	public CurrentAccount()
	{
		
	}
	public CurrentAccount(String customerName, int accountNo, int currentBalance)
	{
		super();
		this.currentBalance = currentBalance;
	}
	
	void display()
	{
		System.out.print(currentBalance);
		super.display();
	}
	

}
