package com.basics.day4;

public class AccountDetails extends CurrentAccount{
	private int depositAmount;
	private int withdrawalAmount;
	
	public AccountDetails(String customerName, int accountNo, int currentBalance,int depositAmount, int withdrawalAmount)
	{
		super(customerName, accountNo, currentBalance);
		this.depositAmount = depositAmount;
		this.withdrawalAmount = withdrawalAmount;
	}
	
	void display()
	{
		System.out.println(depositAmount+"::"+withdrawalAmount);
		super.display();
	}
}
