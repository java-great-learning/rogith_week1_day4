package com.basics.day4;
import java.util.Scanner;

public class Reverse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num,rev=0;
		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();
		
		while(num !=0)
		{
			int rem = num%10;
			rev = rev*10 + rem;
			num = num/10;
		}
		System.out.println(rev);

	}

}
